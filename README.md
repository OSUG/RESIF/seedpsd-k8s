Deployment of seedpsd webservice and workers


Staging:

    helm secrets upgrade -i staging-seedpsd seedpsd -f helm-values/staging/values.yaml -f helm-values/staging/secrets.yaml -f https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml 

Production:

    helm secrets upgrade -i production-seedpsd seedpsd -f helm-values/production/values.yaml -f helm-values/production/secrets.yaml -f https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml 


There is an upgrade-hook that manages database migration in a kubnernetes job.
